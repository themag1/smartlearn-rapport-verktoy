# Deilig Powershell
En samling av scripts som har vært eller er nyttige for meg. Samlet her for deling og hygge og historisk verdi!

## Oversikt-over-filer.ps1
Oversikt over filer i en mappe, med full bane, størrelse, sist lagret dato og lengde på filbane.


## Smartlearn rapport-verktøy 
Ikke lenger aktuelt å bruke av naturlige årsaker ¯\\\_(ツ)_/¯

Skript som rydder opp i rapporter fra SmartLearn
SmartLearn-Rapport-verktøy.ps1 er et såkalt PowerShell-skript som hjelper til å rydde opp i (visse) rapporter fra SmartLearn. PowerShell er et skript-verktøy som følger med Windows.

![](Smartlearn-rapport-verktøy.mp4)

### Scriptet gjør følgende
* Lar brukeren velge SmartLearn-rapport (CSV- eller TXT-fil) - se forutsetninger ved manuell eksport under 
* Åpner Dokumenter-mappa som standard
* Fjerner alle kolonner bortsett fra CompanyName, org_name, title, lastname, userName & person_passed (dette kan man lett endre på i selve skriptet)
* Fjerner alle rader som ikke inneholder et username
* Formaterer utvalget som tabell i Excel, og tilpasser kolonnebreddene
* Åpner generert fil i excel
* Lagrer generert excel-fil som SmartLearn-Rapport-ÅR_MÅNED_DAG_TIME_MINUTT_SEKUND.xlsx

### Forutsetninger ved eksport fra SmartLearn
* Ta med ekstra personopplysninger: Brukernavn må være med
* Visning: Ekspandert
* Format: CSV
