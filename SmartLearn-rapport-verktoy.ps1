# Script som rydder opp i rapporter fra SmartLearn.
# Laget: 29.12.2020
# Laget av Magnus Lundberg (lunmag@so-hf.no), Fag- og Kompetanseavdelingen, Sykehuset Østfold
#
# !!! OBS !!!
# Skriptet må kjøres lokalt fra egen PC, ikke fra f.eks. et fellesområde!
# Anbefaler å legge det i Dokumenter-mappa :-)
#
# ENDRINGSLOGG til og med 21.11.2022
#
# 21.11.2022
# - Lagt skriptet på gitlab 
#
# 11.11.2022
# - Kommentert ut "$excel.Quit()" rundt linje 75, da denne kanskje fikk skriptet til å feile innimellom
#
# 20.1.2022
# - Endret litt i tekstboks-teksten
# - Endret standard mappe til Dokumenter
# - Standard filtype i åpningsdialogboks endret til TXT
#
# 31.3.2022
# - Fjernet første linje med "Add-Type -AssemblyName "Microsoft.Office.Interop.Excel" da den lå dobbelt
# - Lagt til 2>$null
#
# 13.10.2022
# - Lagt til OBS om at skriptet må kjøres fra lokal mappe
# - Endret informasjon om hva skriptet gjør og at Dokumenter-mappen åpnes som standard
# - Prøvd å finne hvor jeg har lagt til "2>$null", men dette må ha vært en test som ikke leverte som den skulle
#
#
$timestamp = Get-date -Format yyyyMMdd-HHmmss
$workdir = $HOME+'\Documents\'
$tmp1 = $workdir+'tmp1-'+$timestamp+'.csv'
$tmp2 = $workdir+$timestamp+'.csv' # navnet på denne blir navnet på arbeidsboken i Excel
$rapport = $workdir+'SmartLearn-Rapport-'+$timestamp+'.xlsx'

#Add-Type -AssemblyName "Microsoft.Office.Interop.Excel"
Add-Type -AssemblyName System.Windows.Forms 
Add-Type -AssemblyName "Microsoft.Office.Interop.Excel" 
#
# Dialogboks!
#
$ButtonType   = [System.Windows.Forms.MessageBoxButtons]::OK
$MessageIcon  = [System.Windows.Forms.MessageBoxIcon]::Information
$MessageBody  = "Dette programmet lar deg gjøre om rapporter fra SmartLearn til Excel-format. Velg .csv/.txt fra SmartLearn i neste vindu og trykk åpne`n`nNår filen åpnes i Excel er alt perfekt`n`nLurt å fjerne duplikater via Excel`n`nRapporten lagrer seg i Dokumenter-mappa som SmartLearn-Rapport-ÅR_MÅNED_DAG_TIME_MINUTT_SEKUND.xlsx.`n`nFor eksempel en raport laget klokken 18.04.22 lille julaften 2020: SmartLearn-Rapport-20201223-180422.xlsx"
$MessageTitle = "♥ Det beste i livet er automagisk ♥"
$Result       = [System.Windows.Forms.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
switch  ($Result) {

  'OK' {
$FileBrowser = New-Object System.Windows.Forms.OpenFileDialog -Property @{
    #InitialDirectory = [Environment]::GetFolderPath('MyComputer')
    InitialDirectory = $workdir
    Filter = 'TXT- og CSV-filer|*.txt;*.csv|Alle filer|*.*|CSV-filer (*.csv)|*.csv'
}
[void]$FileBrowser.ShowDialog() 

Import-Csv $FileBrowser.FileNames | select CompanyName,org_name,title,lastname,userName,person_passed | Export-Csv -Path $tmp1 –NoTypeInformation -Encoding UTF8
import-csv $tmp1 | where-object {$_.username -GT 0} | Export-Csv -UseCulture -Path $tmp2 –NoTypeInformation -Encoding UTF8

$excel = New-Object -ComObject Excel.Application 
$excel.Visible = $false
$excel.Workbooks.Open($tmp2).SaveAs($rapport,51)
#$excel.Quit()
$wb = $excel.Workbooks.Open($rapport)
$excel.ActiveSheet.Columns.AutoFit()
$list = $excel.ActiveSheet.ListObjects.Add(
            [Microsoft.Office.Interop.Excel.XlListObjectSourceType]::xlSrcRange, # Add a range
            $excel.ActiveCell.CurrentRegion, # Get the current region, by default A1 is selected so it'll select all contiguous rows
            $null,
            [Microsoft.Office.Interop.Excel.XlYesNoGuess]::xlYes # Yes, we have a header row
        )
$excel.DisplayAlerts = $false # Ignore / hide alerts
$excel.ActiveWorkbook.SaveAs(
            $rapport, # CHANGE_ME: Output filename
            [Microsoft.Office.Interop.Excel.XlFileFormat]::xlWorkbookDefault, # Save as .xlsx
            $null,
            $null,
            $null,
            $false, # Do not create backup
            $null,
            [Microsoft.Office.Interop.Excel.XlSaveConflictResolution]::xlLocalSessionChanges # Do not prompt for changes
        )
$excel.Visible = $true # CHANGE_ME: Set to false to hide Excel
#$excel.Quit()
  }

  }
  rm $tmp1
  rm $tmp2
