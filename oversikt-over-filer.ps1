$timestamp = Get-date -Format yyyyMMdd
$workdir = $PWD | select -Expand Path
$tmp1 = $workdir+'\'+'tmp1-'+$timestamp+'.csv'
$rapport = $workdir+'\'+$timestamp+'-liste over filer.xlsx'

Add-Type -AssemblyName "Microsoft.Office.Interop.Excel"

Get-ChildItem -Recurse | Where-Object { $_.PSIsContainer -eq $false } |
Select-Object @{Name='Filnavn og bane'; Expression={$_.FullName}}, @{Name='Size (MB)'; Expression={$_.Length / 1MB}}, @{Name='Sist lagret'; Expression={$_.LastWriteTime}}, @{Name='Banelengde'; Expression={$_.FullName.Length}} |
Export-Csv -Delimiter ";" -Path $tmp1 -NoTypeInformation -Encoding UTF8

$excel = New-Object -ComObject Excel.Application 
$excel.Visible = $false
$excel.Workbooks.Open($tmp1).SaveAs($rapport,51)
$wb = $excel.Workbooks.Open($rapport)
$excel.ActiveSheet.Columns.AutoFit()
$list = $excel.ActiveSheet.ListObjects.Add(
            [Microsoft.Office.Interop.Excel.XlListObjectSourceType]::xlSrcRange, 
            $excel.ActiveCell.CurrentRegion, 
            $null,
            [Microsoft.Office.Interop.Excel.XlYesNoGuess]::xlYes 
        )
$excel.DisplayAlerts = $false 
$excel.ActiveWorkbook.SaveAs(
            $rapport, # CHANGE_ME: Output filename
            [Microsoft.Office.Interop.Excel.XlFileFormat]::xlWorkbookDefault, 
            $null,
            $null,
            $null,
            $false,
            $null,
            [Microsoft.Office.Interop.Excel.XlSaveConflictResolution]::xlLocalSessionChanges 
        )
$excel.Visible = $true 

rm $tmp1